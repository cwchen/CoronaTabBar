-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require("widget")

-- Set background to beige.
display.setDefault("background", 245 / 255, 245 / 255, 220 / 255)

local text1 = display.newText(
  {
    text = "Page 1",
    x = display.contentWidth * 0.5,
    y = display.contentHeight * 0.45,
    fontSize = 20,
  }
)

text1:setFillColor(0 / 255, 0 / 255, 0 / 255)
text1.isVisible = true

local text2 = display.newText(
  {
    text = "Page 2",
    x = display.contentWidth * 0.5,
    y = display.contentHeight * 0.45,
    fontSize = 20,
  }
)

text2:setFillColor(0 / 255, 0 / 255, 0 / 255)
text2.isVisible = false

local text3 = display.newText(
  {
    text = "Page 3",
    x = display.contentWidth * 0.5,
    y = display.contentHeight * 0.45,
    fontSize = 20,
  }
)

text3:setFillColor(0 / 255, 0 / 255, 0 / 255)
text3.isVisible = false

-- Configure the tab buttons to appear within the bar
local tabButtons = {
  {
    label = "Tab1",
    id = "tab1",
    size = 20,
    selected = true,
    onPress = function ()
      text1.isVisible = true
      text2.isVisible = false
      text3.isVisible = false
    end
  },
  {
    label = "Tab2",
    id = "tab2",
    size = 20,
    onPress = function ()
      text1.isVisible = false
      text2.isVisible = true
      text3.isVisible = false
    end
  },
  {
    label = "Tab3",
    id = "tab3",
    size = 20,
    onPress = function ()
      text1.isVisible = false
      text2.isVisible = false
      text3.isVisible = true
    end
  }
}

-- Create the widget
local tabBar = widget.newTabBar(
  {
    top = display.contentHeight - 100,
    width = display.contentWidth,
    buttons = tabButtons
  }
)
